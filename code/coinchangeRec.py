import timeit
import sys
import ast

def count_coins(coins, value):
        flag = None
        for c in coins:
        	if c == value:
        		return c
        	if c < value:
        		flag = c
        temp = round(value - flag, 2)
        return [flag] + [count_coins(coins, temp)]

output = []
def non_nested(min_coin):
        for i in min_coin:
                if type(i) == list:
                        non_nested(i)
                else:
                        output.append(i)
        return output

def min_count_coins(coins, value):
        min_coin = count_coins(coins, value)
        res = non_nested(min_coin)
        list_coins = res[::-1]
        return list_coins


# Driver progarm
coins = ast.literal_eval(sys.argv[3])
value = int(sys.argv[1])
size = int(sys.argv[2])
start = timeit.default_timer()
list_coins = min_count_coins(coins, value)
time = timeit.default_timer() - start
print("{0} \n {1} seconds ".format(list_coins,time))

# input is cmd line in format- python coinchangeRec.py 93 4 "[1,5,10,25]"
