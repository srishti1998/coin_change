import timeit
import sys
import ast

def min_count_coins(coins, value): 
    tab = [0] * (value + 1)
    coinUsed = [0] * (value + 1)
    for i in range(value + 1):
        temp = i
        newCoin = 1
        for j in [c for c in coins if c <= i]:
            if tab[i - j]  + 1 < temp:
                temp = tab[i - j]  + 1
                newCoin = j
        tab[i] = temp
        coinUsed[i] = newCoin
    List_Coins = printCoins(coinUsed, value)
    return List_Coins

def printCoins(coinUsed,value):
    coinArr = []
    coin = value
    while coin > 0:
        temp = coinUsed[coin]
        coinArr.append(temp)
        coin = coin - temp
    return coinArr
        

# Driver progarm
coins = ast.literal_eval(sys.argv[3])
value = int(sys.argv[1])
size = int(sys.argv[2])
start = timeit.default_timer()
list_coins = min_count_coins(coins, value)
time = timeit.default_timer() - start
print("{0} \n {1} seconds ".format(list_coins,time))

# input is cmd line in format- python coinchangeRec.py 93 4 "[1,5,10,25]"
