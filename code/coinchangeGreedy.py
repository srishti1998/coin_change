import timeit
import sys
import ast

def min_count_coins(coins, value):
	n = len(coins)
	i = n - 1
	List_Coins = []
	while( i >= 0):
		while( value >= coins[i]):
			value -= coins[i]
			List_Coins.append(coins[i])
		i -= 1

	return List_Coins



# Driver progarm
coins = ast.literal_eval(sys.argv[3])
value = int(sys.argv[1])
size = int(sys.argv[2])
start = timeit.default_timer()
list_coins = min_count_coins(coins, value)
time = timeit.default_timer() - start
print("{0} \n {1} seconds ".format(list_coins,time))
